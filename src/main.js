import Vue from 'vue'
import VueRouter from 'vue-router'
import App from './App.vue'
import 'bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import ChartGrid from "./components/ChartGrid.vue"

Vue.config.productionTip = false
Vue.use(VueRouter)


const router = new VueRouter({
  routes:[
    {
      path: '/',
      component: ChartGrid
    }
  ]
})

new Vue({
  render: h => h(App),
  router
}).$mount('#app')
